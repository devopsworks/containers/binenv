FROM debian:buster-slim AS builder
LABEL maintainer "Devops.Works <support@devops.works>"

ENV ARCH="amd64"

WORKDIR /build

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y curl wget jq && \
    curl -s https://api.github.com/repos/devops-works/binenv/releases/latest | jq -r ".assets[] | select(.name | contains(\"linux_${ARCH}\")) | .browser_download_url" | wget -qi - -O binenv 

FROM debian:buster-slim
COPY --from=builder /build/binenv .

ENV PATH=/root/.binenv:$PATH

RUN apt-get update && \
    apt-get install -y ca-certificates && \
    rm -rf /var/lib/apt/lists/ && \
    chmod +x binenv && \
    ./binenv update binenv && \
    ./binenv install binenv && \
    binenv update -c && \
    rm binenv

ENTRYPOINT [ "/bin/bash" ]
