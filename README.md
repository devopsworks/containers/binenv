# Binenv

Simple & stupid Docker image for [binenv](https://github.com/devops-works/binenv/pulls).

This is the best way to get a functional shell with Binenv, for example to debug something in kubernetes.

## Run it into Kubernetes

Launch a pod

```bash
kubectl run -it --rm --restart=Never --image=registry.gitlab.com/devopsworks/containers/binenv binenv
```

Now you can easily install the tool you need (e.g. `vmctl` to import metrics into Victoria Metrics)

```bash
If you don't see a command prompt, try pressing enter.
root@binenv:/# binenv install vmctl
root@binenv:/# vmctl influx --influx-addr http://influxdb.blah:8086 --influx-database foobar --influx-filter-time-start '2020-10-08T20:07:00Z'  --vm-addr http://victoria.namespace:8428 --influx-concurrency 5
```

